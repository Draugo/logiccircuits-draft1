package draugo.logic.block;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import draugo.logic.resources.ModInfo;

public class BlockColor extends Block {
	public static int BLOCK_ID;
	public static final String CONFIG_KEY = "block.color";
	public static final int DEFAULT_ID = 2077;
	
	public static final String UNLOCALIZED_NAME = "draugo.logic.block.color";
	public static final String NAME = "Color Deployer";
	
	public static final String TEXTURE = "color";
	public static final String[] TEXTURE_COLORS = {"_white", "_red", "_green", "_blue"};
	public static final String TEXTURE_FRONT = "_front";
	public static final String TEXTURE_SIDE = "_side";
	
	public BlockColor(int id) {
		super(id, Material.iron);
		setStepSound(Block.soundMetalFootstep);
		setHardness(1F);
		setCreativeTab(CreativeTabs.tabRedstone);
		setUnlocalizedName(UNLOCALIZED_NAME);
	}
	
	@SideOnly(Side.CLIENT)
	private Icon[] frontIcons;
	@SideOnly(Side.CLIENT)
	private Icon[] sideIcons;
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register) {
		frontIcons = new Icon[TEXTURE_COLORS.length];
		for(int i=0; i<frontIcons.length; i++) {
			frontIcons[i] = register.registerIcon(ModInfo.ASSET_LOCATION + ":" + TEXTURE + TEXTURE_COLORS[i] + TEXTURE_FRONT);
		}
		sideIcons = new Icon[TEXTURE_COLORS.length];
		for(int i=0; i<frontIcons.length; i++) {
			sideIcons[i] = register.registerIcon(ModInfo.ASSET_LOCATION + ":" + TEXTURE + TEXTURE_COLORS[i] + TEXTURE_SIDE);
		}
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public Icon getIcon(int side, int meta) {
		int color = meta & 0b0011;
		if(side == 0 || side == 1) {
			return sideIcons[color];
		} else {
			side -= 2;
			int frontSide = (meta & 0b1100) >> 2;
			return (side == frontSide) ? frontIcons[color] : sideIcons[color];
		}
	}
	
	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
		if(side > 1) {
			if(!world.isRemote) {
				int meta = world.getBlockMetadata(x, y, z);
				
				meta &= ~0b1100;
				meta |= (side - 2) << 2;
				
				world.setBlockMetadataWithNotify(x, y, z, meta, 3);
			}
			
			
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, int id) {
		if(!world.isRemote && world.isBlockIndirectlyGettingPowered(x, y, z)) {
			int meta = world.getBlockMetadata(x, y, z);
			int side = (meta & 0b1100) >> 2;
			int targetX = x;
			int targetZ = z;
			switch(side) {
			case 0:
				targetZ--;
				break;
			case 1:
				targetZ++;
				break;
			case 2:
				targetX--;
				break;
			case 3:
				targetX++;
				break;
			}
			
			int woolMeta = 0;
			int color = meta & 0b0011;
			switch(color) {
			case 0:
				woolMeta = 0;
				break;
			case 1:
				woolMeta = 14;
				break;
			case 2:
				woolMeta = 13;
				break;
			case 3:
				woolMeta = 11;
				break;
			}
			
			world.setBlock(targetX, y, targetZ, Block.cloth.blockID, woolMeta, 3);
		}
	}
}
