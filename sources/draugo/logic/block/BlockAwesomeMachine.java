package draugo.logic.block;

import java.util.List;
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Icon;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import cpw.mods.fml.common.network.FMLNetworkHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import draugo.logic.LogicCircuitsFirstDraft;
import draugo.logic.client.RenderAwesomeMachine;
import draugo.logic.resources.ModInfo;
import draugo.logic.tileentities.TileEntityAwesomeMachine;

public class BlockAwesomeMachine extends BlockContainer {
	public static int BLOCK_ID;
	public static final String CONFIG_KEY = "block.awesomemachine";
	public static final int DEFAULT_ID = 2075;
	
	public static final String UNLOCALIZED_NAME = "draugo.logic.block.awesomeMachine";
	public static final String NAME = "Awesome machine";
	
	public static final String TEXTURE_TOP = "awesome_machine_top";
	public static final String TEXTURE_TOP_DISABLED = "awesome_machine_top_disabled";
	public static final String[] TEXTURE_SIDES = {
															"awesome_machine_side",
															"awesome_machine_side_left",
															"awesome_machine_side_equal",
															"awesome_machine_side_right",
															"awesome_machine_side_custom"};
	public static final String[] TEXTURE_SIDES_DISABLED = {
																	"awesome_machine_side_disabled",
																	"awesome_machine_side_left_disabled",
																	"awesome_machine_side_equal_disabled",
																	"awesome_machine_side_right_disabled",
																	"awesome_machine_side_custom_disabled"};
	public static final String TEXTURE_BOTTOM = "awesome_machine_bottom";
	
	public BlockAwesomeMachine(int id) {
		super(id, Material.iron);
		
		setCreativeTab(CreativeTabs.tabRedstone);
		setHardness(2F);
		setStepSound(Block.soundMetalFootstep);
		setUnlocalizedName(UNLOCALIZED_NAME);
	}
	
	@SideOnly(Side.CLIENT)
	private Icon topIcon;
	@SideOnly(Side.CLIENT)
	private Icon topIconDisabled;
	@SideOnly(Side.CLIENT)
	private Icon bottomIcon;
	@SideOnly(Side.CLIENT)
	private Icon[] sideIcons;
	@SideOnly(Side.CLIENT)
	private Icon[] sideIconsDisabled;
	
	@SideOnly(Side.CLIENT)
	@Override
	public void registerIcons(IconRegister register) {
		topIcon = register.registerIcon(ModInfo.ASSET_LOCATION + ":" + TEXTURE_TOP);
		topIconDisabled = register.registerIcon(ModInfo.ASSET_LOCATION + ":" + TEXTURE_TOP_DISABLED);
		bottomIcon = register.registerIcon(ModInfo.ASSET_LOCATION + ":" + TEXTURE_BOTTOM);
		
		sideIcons = new Icon[TEXTURE_SIDES.length];
		for(int i = 0; i < sideIcons.length; i++)
			sideIcons[i] = register.registerIcon(ModInfo.ASSET_LOCATION + ":" + TEXTURE_SIDES[i]);

		sideIconsDisabled = new Icon[TEXTURE_SIDES_DISABLED.length];
		for(int i = 0; i < sideIconsDisabled.length; i++)
			sideIconsDisabled[i] = register.registerIcon(ModInfo.ASSET_LOCATION + ":" + TEXTURE_SIDES_DISABLED[i]);
	}
	
	// Old way
	private boolean isEnabled(int meta) {
		return (meta % 2) == 1;
	}
	// New way
	/*private boolean isEnabled(TileEntityAwesomeMachine machine) {
		return machine.isEnabled();
	}*/
	
	// Old way
	@SideOnly(Side.CLIENT)
	@Override
	public Icon getIcon(int side, int meta) {
		if (side == 0) {
			return bottomIcon;
		}else if(side == 1) {
			return isEnabled(meta) ? topIcon : topIconDisabled;
		}else{
			int type = meta / 2;
			return isEnabled(meta) ? sideIcons[type] : sideIconsDisabled[type];
		}
	}
	
	// New Way
	/*@Override
	@SideOnly(Side.CLIENT)
	public Icon getBlockTexture(IBlockAccess blockAccess, int x, int y, int z, int side) {
		int meta = blockAccess.getBlockMetadata(x, y, z);
		if(side == 0) {
			return bottomIcon;
		} else if(side == 1) {
			TileEntity te = blockAccess.getBlockTileEntity(x, y, z);
			if(te != null && te instanceof TileEntityAwesomeMachine) {
				return (isEnabled((TileEntityAwesomeMachine)te))?topIcon:topIconDisabled;
			} else {
				return null;
			}
		} else {
			TileEntity te = blockAccess.getBlockTileEntity(x, y, z);
			if(te != null && te instanceof TileEntityAwesomeMachine) {
				TileEntityAwesomeMachine machine = (TileEntityAwesomeMachine) te;
				if(isEnabled(machine))
					return sideIcons[machine.getType()];
				else
					return sideIconsDisabled[machine.getType()];
			} else {
				return null;
			}
		}
	}*/
	
	@Override
	public int damageDropped(int meta) {
		return meta;
	}
	
	@Override
	public void onEntityWalking(World world, int x, int y, int z, Entity entity) {
		int meta = world.getBlockMetadata(x, y, z);
		if(!world.isRemote) {
			TileEntity te = world.getBlockTileEntity(x, y, z);
			if(te != null && te instanceof TileEntityAwesomeMachine) {
				TileEntityAwesomeMachine machine = (TileEntityAwesomeMachine) te;
				// Old way
				if(isEnabled(meta))
				// New way
				//if(isEnabled(machine))
					spawnAnvil(world, machine, x, y, z);
			}
		}
	}
	
	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, int id) {
		int meta = world.getBlockMetadata(x, y, z);
		if(!world.isRemote && world.isBlockIndirectlyGettingPowered(x,  y, z)) {
			TileEntity te = world.getBlockTileEntity(x, y, z);
			if(te != null && te instanceof TileEntityAwesomeMachine) {
				TileEntityAwesomeMachine machine = (TileEntityAwesomeMachine) te;
				// Old way
				if(isEnabled(meta)) {
				// New Way
				//if(isEnabled(machine)) {
					switch(machine.getType()) {
					case 1:
						for(int i = -4; i <= 4; i++) {
							for(int j = -4; j <= 4; j++) {
								if(i != 0 || j != 0) {
									if(Math.abs(i) > 2 && j == 0)
										spawnAnvil(world, machine, x+i, y, z+j);
									if(Math.abs(j) > 2 && i == 0)
										spawnAnvil(world, machine, x+i, y, z+j);
									
									if(i*j<0 && Math.abs(i) < 4 && Math.abs(j) == 1)
										spawnAnvil(world, machine, x+i, y, z+j);
									
									if(i*j>0 && Math.abs(j) < 4 && Math.abs(i) == 1)
										spawnAnvil(world, machine, x+i, y, z+j);
								}
							}
						}
						break;
					case 2:
						for(int i = -1; i <= 1; i++) {
							for(int j = -1; j <= 1; j++) {
								if(i != 0 || j != 0)
									spawnAnvil(world, machine, x+i, y, z+j);
							}
						}
						break;
					case 3:
						for(int i = -4; i <= 4; i++) {
							for(int j = -4; j <= 4; j++) {
								if(i != 0 || j != 0) {
									if(Math.abs(i) > 2 && j == 0)
										spawnAnvil(world, machine, x+i, y, z+j);
									if(Math.abs(j) > 2 && i == 0)
										spawnAnvil(world, machine, x+i, y, z+j);
									
									if(i*j<0 && Math.abs(j) < 4 && Math.abs(i) == 1)
										spawnAnvil(world, machine, x+i, y, z+j);
									
									if(i*j>0 && Math.abs(i) < 4 && Math.abs(j) == 1)
										spawnAnvil(world, machine, x+i, y, z+j);
								}
							}
						}
						break;
					case 4:
						for(int i = 0; i < machine.getCustomSetup().length; i++) {
							if(machine.getCustomSetup()[i] == 1) {
								int dropX = x + i % 7 - 3;
								int dropZ = z + i / 7 - 3;
								
								spawnAnvil(world, machine, dropX, y, dropZ);
							}
						}
					}
				}
			}
		}
	}
	
	@Override
	public void updateTick(World world, int x, int y, int z, Random rand) {
		super.updateTick(world, x, y, z, rand);
		if(world.isRemote) {
			world.markBlockForRenderUpdate(x, y, z);
		}
	}
	
	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
		if(!world.isRemote) {
			FMLNetworkHandler.openGui(player, LogicCircuitsFirstDraft.instance, 0, world, x, y, z);
		}
		
		return true;
	}
	
	private void spawnAnvil(World world, TileEntityAwesomeMachine machine, int x, int y, int z) {
		if(world.isAirBlock(x, y + machine.heightSetting, z)) {
			for(int i = 0; i < machine.getSizeInventory(); i++) {
				ItemStack stack = machine.getStackInSlot(i);
				if(stack != null && stack.itemID == Block.anvil.blockID) {
					machine.decrStackSize(i, 1);
					world.setBlock(x, y + machine.heightSetting, z, Block.anvil.blockID);
					return;
				}
			}
		}
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void getSubBlocks(int id, CreativeTabs tab, List list) {
		for(int i = 0; i < TEXTURE_SIDES.length; i++) {
			ItemStack item = new ItemStack(this, 1);
			
			// Old way
			item.setItemDamage(i*2);
			
			// New Way
			if(!item.hasTagCompound()) item.setTagCompound(new NBTTagCompound());
			item.getTagCompound().setByte(TileEntityAwesomeMachine.TAG_TYPE, (byte)i);
			item.getTagCompound().setBoolean(TileEntityAwesomeMachine.TAG_ENABLED, false);
			list.add(item);
		}
	}

	@Override
	public TileEntity createNewTileEntity(World world) {
		return new TileEntityAwesomeMachine();
	}
	
	@Override
	public void breakBlock(World world, int x, int y, int z, int id, int meta) {
		TileEntity te = world.getBlockTileEntity(x, y, z);
		if(te != null && te instanceof IInventory) {
			TileEntityAwesomeMachine machine = (TileEntityAwesomeMachine) te;
			for(int i = 0; i < machine.getSizeInventory(); i++) {
				ItemStack stack = machine.getStackInSlotOnClosing(i);
				if(stack != null) {
					float spawnX = x + world.rand.nextFloat();
					float spawnY = y + world.rand.nextFloat();
					float spawnZ = z + world.rand.nextFloat();
					
					EntityItem item = new EntityItem(world, spawnX, spawnY, spawnZ, stack);
					
					float slow = 0.05F;
					
					item.motionX = (-0.5F + world.rand.nextFloat()) * slow;
					item.motionY = (4 + world.rand.nextFloat()) * slow;
					item.motionZ = (-0.5F + world.rand.nextFloat()) * slow;
					
					world.spawnEntityInWorld(item);
				}
			}
		}
				
		super.breakBlock(world, x, y, z, id, meta);
	}
	
	@Override
	public void setBlockBoundsBasedOnState(IBlockAccess world, int x, int y, int z) {
		if(world.getBlockMetadata(x, y, z) % 2 == 1) {
			setBlockBounds(0F, 0F, 0F, 1F, 1F, 1F);
		} else {
			setBlockBounds(0.25F, 0.25F, 0.25F, 0.75F, 0.75F, 0.75F);
		}
	}
	
	@Override
	public boolean isOpaqueCube() {
		return false;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public AxisAlignedBB getSelectedBoundingBoxFromPool(World world, int x, int y, int z) {
		setBlockBoundsBasedOnState(world, x, y, z);
		return super.getSelectedBoundingBoxFromPool(world, x, y, z);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int x, int y, int z) {
		setBlockBoundsBasedOnState(world, x, y, z);
		return super.getCollisionBoundingBoxFromPool(world, x, y, z);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public MovingObjectPosition collisionRayTrace(World world, int x, int y, int z, Vec3 start, Vec3 end) {
		setBlockBoundsBasedOnState(world, x, y, z);
		return super.collisionRayTrace(world, x, y, z, start, end);
	}
	
	@Override
	public void setBlockBoundsForItemRender() {
		setBlockBounds(0F, 0F, 0F, 1F, 1F, 1F);
	}
	
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}
	
	@Override
	public int getRenderType() {
		return RenderAwesomeMachine.RENDER_ID;
	}
}
