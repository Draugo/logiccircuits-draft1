package draugo.logic.block;

import net.minecraft.block.Block;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import draugo.logic.items.ItemAwesomeMachine;
import draugo.logic.tileentities.TileEntityAwesomeMachine;
import draugo.logic.tileentities.TileEntityBombBox;

public class BlockHandler {
	public static Block awesomemachine;
	public static Block bombbox;
	public static BlockPoison poison;
	public static BlockColor color;
	
	public static void init() {
		awesomemachine = new BlockAwesomeMachine(BlockAwesomeMachine.BLOCK_ID);
		GameRegistry.registerBlock(awesomemachine, ItemAwesomeMachine.class, BlockAwesomeMachine.CONFIG_KEY);
		
		bombbox = new BlockBombBox(BlockBombBox.BLOCK_ID);
		GameRegistry.registerBlock(bombbox, BlockBombBox.CONFIG_KEY);
		
		poison = new BlockPoison(BlockPoison.BLOCK_ID);
		GameRegistry.registerBlock(poison, BlockPoison.CONFIG_KEY);
		
		color = new BlockColor(BlockColor.BLOCK_ID);
		GameRegistry.registerBlock(color, BlockColor.CONFIG_KEY);
	}
	
	public static void addNames() {
		LanguageRegistry.addName(awesomemachine, BlockAwesomeMachine.NAME);
		LanguageRegistry.addName(bombbox, BlockBombBox.NAME);
		LanguageRegistry.addName(poison, BlockPoison.NAME);
		LanguageRegistry.addName(color, BlockColor.NAME);
	}
	
	public static void registerTileEnetities() {
		GameRegistry.registerTileEntity(TileEntityBombBox.class, TileEntityBombBox.TE_KEY);
		GameRegistry.registerTileEntity(TileEntityAwesomeMachine.class, TileEntityAwesomeMachine.TE_KEY);
	}
}
