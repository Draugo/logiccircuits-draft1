package draugo.logic.block;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import draugo.logic.particles.Particles;
import draugo.logic.resources.ModInfo;

public class BlockPoison extends Block {
	public static int BLOCK_ID;
	public static final String CONFIG_KEY = "block.poisonblock";
	public static final int DEFAULT_ID = 2077;
	
	public static final String UNLOCALIZED_NAME = "draugo.logic.block.poisonblock";
	public static final String NAME = "Block of Poison";
	
	public static final String TEXTURE = "poison_block";
	public static final String PARTICLE = "poison_particle";
	
	public BlockPoison(int id) {
		super(id, Material.rock);
		
		setCreativeTab(CreativeTabs.tabBlock);
		setHardness(1F);
		setStepSound(Block.soundStoneFootstep);
		setUnlocalizedName(UNLOCALIZED_NAME);
	}
	
	@SideOnly(Side.CLIENT)
	public Icon poisonParticle;
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register) {
		blockIcon = register.registerIcon(ModInfo.ASSET_LOCATION + ":" + TEXTURE);
		poisonParticle = register.registerIcon(ModInfo.ASSET_LOCATION + ":" + PARTICLE);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void randomDisplayTick(World world, int x, int y, int z, Random rand) {
		for(int i = 0; i< 4; i++) {
			
			float particleX = x + rand.nextFloat();
			float particleY = y + rand.nextFloat();
			float particleZ = z + rand.nextFloat();
			
			float moveX = -0.5F + rand.nextFloat();
			float moveY = -0.5F + rand.nextFloat();
			float moveZ = -0.5F + rand.nextFloat();
			
			Particles.POISON.spawnParticle(world, particleX, particleY, particleZ, moveX, moveY, moveZ);
			
			//world.spawnParticle("portal", particleX, particleY, particleZ, moveX, moveY, moveZ); // Vanilla particle
		}
	}
}
