package draugo.logic.block;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import draugo.logic.resources.ModInfo;
import draugo.logic.tileentities.TileEntityBombBox;

public class BlockBombBox extends BlockContainer {
	public static int BLOCK_ID;
	public static final String CONFIG_KEY = "block.bombbox";
	public static final int DEFAULT_ID = 2076;
	
	public static final String UNLOCALIZED_NAME = "draugo.logic.block.bombbox";
	public static final String NAME = "Bomb box";
	
	public static final String TEXTURE = "bomb_box";
	public static final String TEXTURE_EXPLODED = "bomb_box_exploded";
	
	public BlockBombBox(int id) {
		super(id, Material.iron);
		
		setCreativeTab(CreativeTabs.tabRedstone);
		setHardness(2F);
		setStepSound(Block.soundMetalFootstep);
		setUnlocalizedName(UNLOCALIZED_NAME);
	}
	
	@SideOnly(Side.CLIENT)
	private Icon explodedTexture;

	@SideOnly(Side.CLIENT)
	@Override
	public void registerIcons(IconRegister register) {
		blockIcon = register.registerIcon(ModInfo.ASSET_LOCATION + ":" + TEXTURE);
		explodedTexture = register.registerIcon(ModInfo.ASSET_LOCATION + ":" + TEXTURE_EXPLODED);
	}

	@Override
	public TileEntity createNewTileEntity(World world) {
		return new TileEntityBombBox();
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public Icon getBlockTexture(IBlockAccess world, int x, int y, int z, int side) {
		TileEntityBombBox te = (TileEntityBombBox) world.getBlockTileEntity(x, y, z);
		return (te.isExploded()) ? explodedTexture : blockIcon;
	}
	
	@Override
	public int idDropped(int meta, Random random, int fortune) {
		return -1;
	}
	
	// Custom rendering
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}
	
	@Override
	public boolean isOpaqueCube() {
		return false;
	}
	
	@Override
	public int getRenderType() {
		return -1;
	}
	
	// Not Needed, just for example
	/*
	@SideOnly(Side.CLIENT)
	private Icon texture;
	
	@SideOnly(Side.CLIENT)
	@Override
	public void registerIcons(IconRegister register) {
		texture = register.registerIcon(ModInfo.TEXTURE_LOCATION + ":" + TEXTURE);
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public Icon getIcon(int side, int meta) {
		return texture;
	}
	*/
}
