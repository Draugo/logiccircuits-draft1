package draugo.logic.client;

import org.lwjgl.opengl.GL11;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.world.IBlockAccess;
import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import cpw.mods.fml.client.registry.RenderingRegistry;
import draugo.logic.block.BlockAwesomeMachine;

public class RenderAwesomeMachine implements ISimpleBlockRenderingHandler {

	public static int RENDER_ID;
	
	private int id;
	public RenderAwesomeMachine() {
		id = RenderingRegistry.getNextAvailableRenderId();
	}
	
	@Override
	public void renderInventoryBlock(Block block, int metadata, int modelID, RenderBlocks renderer) {
		block.setBlockBoundsForItemRender();
		renderer.setRenderBoundsFromBlock(block);
		
		GL11.glPushMatrix();
		
		// Default transformations for block rendering
		GL11.glRotatef(90, 0, 1, 0);
		GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
		
		Tessellator tes = Tessellator.instance;
		
		tes.startDrawingQuads();
		tes.setNormal(0F, -1F, 0F);
		renderer.renderFaceYNeg(block, 0, 0, 0, block.getIcon(0, metadata));
		tes.draw();
		
		tes.startDrawingQuads();
		tes.setNormal(0F, 1F, 0F);
		renderer.renderFaceYPos(block, 0, 0, 0, block.getIcon(1, metadata));
		tes.draw();
		
		tes.startDrawingQuads();
		tes.setNormal(0F, 0F, -1F);
		renderer.renderFaceZNeg(block, 0, 0, 0, block.getIcon(2, metadata));
		tes.draw();
		
		tes.startDrawingQuads();
		tes.setNormal(0F, 0F, 1F);
		renderer.renderFaceZPos(block, 0, 0, 0, block.getIcon(3, metadata));
		tes.draw();
		
		tes.startDrawingQuads();
		tes.setNormal(-1F, 0F, 0F);
		renderer.renderFaceXNeg(block, 0, 0, 0, block.getIcon(4, metadata));
		tes.draw();
		
		tes.startDrawingQuads();
		tes.setNormal(1F, 0F, 0F);
		renderer.renderFaceXPos(block, 0, 0, 0, block.getIcon(5, metadata));
		tes.draw();
		
		
		
		GL11.glPopMatrix();
	}

	@Override
	public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer) {
		Tessellator.instance.setColorOpaque_F(1F, 1F, 1F);

		block.setBlockBounds(0F, 0.8F, 0F, 1F, 1F, 1F);
		renderer.setRenderBoundsFromBlock(block);
		renderer.renderStandardBlock(block, x, y, z);
		
		block.setBlockBounds(0F, 0F, 0F, 0.2F, 0.8F, 1F);
		renderer.setRenderBoundsFromBlock(block);
		renderer.renderStandardBlock(block, x, y, z);
		
		block.setBlockBounds(0.8F, 0F, 0F, 1F, 0.8F, 1F);
		renderer.setRenderBoundsFromBlock(block);
		renderer.renderStandardBlock(block, x, y, z);
		
		return true;
	}

	@Override
	public boolean shouldRender3DInInventory() {
		return true;
	}

	@Override
	public int getRenderId() {
		return id;
	}

}
