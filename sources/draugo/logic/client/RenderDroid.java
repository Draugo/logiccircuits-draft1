package draugo.logic.client;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import draugo.logic.entities.EntityDroid;
import draugo.logic.resources.ModInfo;

public class RenderDroid extends Render {
	public static final ResourceLocation texture = new ResourceLocation(ModInfo.ASSET_LOCATION, "textures/models/droid.png");
	
	private ModelDroid model;
	
	public RenderDroid(ModelDroid model) {
		this.model = model;
		shadowSize = 0.5F;
	}
	
	@Override
	public void doRender(Entity entity, double x, double y, double z, float yaw, float partialTickTime) {
		EntityDroid droid = (EntityDroid) entity;
		
		GL11.glPushMatrix();
		
		GL11.glTranslatef((float)x, (float)y, (float)z);
		GL11.glScalef(-1F, -1F, 1F);
		
		// Texture binding
		func_110777_b(droid);
		
		model.render(droid.getCoreRotation(), droid.getSolarPanelRotation(), droid.getOuterSolarPanelRotation(), droid.getHelmetPosition(), droid.getRedColor(), droid.getGreenColor(), droid.getBlueColor(), 0.0625F);
		
		GL11.glPopMatrix();
	}

	@Override
	protected ResourceLocation func_110775_a(Entity entity) {
		return texture;
	}

}
