package draugo.logic.client;

import org.lwjgl.opengl.GL11;

import draugo.logic.tileentities.TileEntityBombBox;

import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.model.IModelCustom;

public class RenderBombBlockItem implements IItemRenderer {

	private IModelCustom model;

	public RenderBombBlockItem(IModelCustom model) {
		this.model = model;
	}
	
	@Override
	public boolean handleRenderType(ItemStack item, ItemRenderType type) {
		return true;
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {
		return true;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
		GL11.glPushMatrix();
		
		switch(type) {
		case EQUIPPED:
			GL11.glTranslatef(0.4F, 1F, 0.6F);
			break;
		case EQUIPPED_FIRST_PERSON:
			GL11.glTranslatef(0, 0.7F, 0.5F);
			GL11.glRotatef(180, 0F, 1F, 0F);
			break;
		default:
			break;
		}
		
		GL11.glScalef(0.5f, 0.5f, 0.5f);
		
		Minecraft.getMinecraft().func_110434_K().func_110577_a(RenderBombBlock.texture);
		model.renderAll();
		
		GL11.glPopMatrix();
	}

}
