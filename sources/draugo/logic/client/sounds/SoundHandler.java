package draugo.logic.client.sounds;

import net.minecraftforge.client.event.sound.SoundLoadEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.ForgeSubscribe;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import draugo.logic.resources.ModInfo;

@SideOnly(Side.CLIENT)
public class SoundHandler {
	
	public SoundHandler() {
		MinecraftForge.EVENT_BUS.register(this);
	}
	
	@ForgeSubscribe
	public void onSoundsLoad(SoundLoadEvent event) {
		for(Sounds sound : Sounds.values()) {
			addSound(event, sound);
		}
	}
	
	private void addSound(SoundLoadEvent event, Sounds sound) {
		event.manager.soundPoolSounds.addSound(ModInfo.ASSET_LOCATION + ":" + sound.getName() + ".ogg");
	}
}
