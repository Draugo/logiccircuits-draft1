package draugo.logic.client;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;

public class RenderDroidItem implements IItemRenderer {

	private ModelDroid model;
	public RenderDroidItem(ModelDroid model) {
		this.model = model;
	}
	
	@Override
	// Does item use this renderer or basic icon renderer
	public boolean handleRenderType(ItemStack item, ItemRenderType type) {
		return true;
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {
		return true;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
		GL11.glPushMatrix();
		
		GL11.glScalef(-1F, -1F, 1F);
		
		switch(type) {
		case INVENTORY:
			GL11.glTranslatef(0,  0.12F,  0);
			break;
		case EQUIPPED:
			GL11.glTranslatef(-0.8F, -0.2F, 0.7F);
			break;
		case EQUIPPED_FIRST_PERSON:
			GL11.glTranslatef(0, -0.7F, 0.7F);
			break;
		}
		
		Minecraft.getMinecraft().func_110434_K().func_110577_a(RenderDroid.texture);
		
		model.render(0, 0, -(float)Math.PI/2, -6, 0.5F, 0, item.stackSize / 64F, 0.0625F);
		
		GL11.glPopMatrix();
	}
	
}
