package draugo.logic.client;

import org.lwjgl.opengl.GL11;

import draugo.logic.resources.ModInfo;
import draugo.logic.tileentities.TileEntityBombBox;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;

public class RenderBombBlock extends TileEntitySpecialRenderer {

	private IModelCustom model;
	public RenderBombBlock(IModelCustom model) {
		this.model = model;
	}
	
	public static final ResourceLocation texture = new ResourceLocation(ModInfo.ASSET_LOCATION, "textures/models/bomb_block.png");
	private static final ResourceLocation textureIdle = new ResourceLocation(ModInfo.ASSET_LOCATION, "textures/models/bomb_block_idle.png");
	
	@Override
	public void renderTileEntityAt(TileEntity tileEntity, double x, double y, double z, float partialTickTime) {
		GL11.glPushMatrix();
		
		GL11.glTranslatef((float)x + 0.5F, (float)y + 0.5F, (float)z + 0.5F);
		GL11.glScalef(0.5f, 0.5f, 0.5f);
		
		Minecraft.getMinecraft().func_110434_K().func_110577_a(((TileEntityBombBox)tileEntity).isExploded() ? textureIdle : texture);
		model.renderAll();
		
		GL11.glPopMatrix();
	}

}
