package draugo.logic.tileentities;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import draugo.logic.block.BlockBombBox;
import draugo.logic.client.sounds.Sounds;

public class TileEntityBombBox extends ExtendedTileEntity {
	public static final String TE_KEY = "tileentity.bombbox";
	
	private static final int SPREAD_TIME = 20;
	private static final int SPREAD_LEVELS = 5;
	
	private int timer;
	private int level;
	
	public TileEntityBombBox() {
		timer = SPREAD_TIME;
		level = 0;
	}
	
	public boolean isExploded() {
		return timer < 0;
	}
	
	@Override
	public void updateEntity() {
		if(!worldObj.isRemote) {
			if(timer == 0)
				worldObj.addBlockEvent(xCoord, yCoord, zCoord, BlockBombBox.BLOCK_ID, 1, 0); // Last two, 1 is event id and 0 is data sent to client.
			if(timer == 0 && level < SPREAD_LEVELS) {
				for(int i = -1; i <= 1; i++)
					for(int j = -1; j <= 1; j++)
						if(Math.abs(i) != Math.abs(j))
							spread(xCoord + i, yCoord, zCoord + j);
				Sounds.BOMB_SPREAD.play(xCoord, yCoord, zCoord, 1, 3);
			} else if(timer == SPREAD_TIME * (level - SPREAD_LEVELS)) {
				worldObj.createExplosion(null, xCoord + 0.5, yCoord + 0.5, zCoord + 0.5, 4, true);
			}
			
			if(timer >= SPREAD_TIME * (level - SPREAD_LEVELS)) timer--;
		}
	}
	
	private void spread(int x, int y, int z) {
		if(worldObj.isAirBlock(x, y, z)) {
			worldObj.setBlock(x, y, z, BlockBombBox.BLOCK_ID);
			
			TileEntity te = worldObj.getBlockTileEntity(x, y, z);
			if(te != null && te instanceof TileEntityBombBox) {
				TileEntityBombBox bb = (TileEntityBombBox) te;
				bb.level = level + 1;
			}
		}
	}
	
	@Override
	public void writeToNBT(NBTTagCompound compound) {
		super.writeToNBT(compound);
		
		compound.setShort("Timer", (short)timer);
		compound.setByte("Level", (byte)level);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound compound) {
		super.readFromNBT(compound);
		
		timer = compound.getShort("Timer");
		level = compound.getByte("Level");
	}

	@Override
	public boolean receiveClientEvent(int id, int value) {
		if(worldObj.isRemote) {
			switch(id) {
			case 1:
				if(value == 0) {
					timer = -1;
				} else {
					timer = 50;
				}
				worldObj.markBlockForRenderUpdate(xCoord, yCoord, zCoord);
				break;
			}
		}
		
		return true;
	}
	
	// Make sure that client has the correct values
	
}
