package draugo.logic.tileentities;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet132TileEntityData;
import net.minecraft.tileentity.TileEntity;

public class ExtendedTileEntity extends TileEntity {
	
	@Override
	public Packet getDescriptionPacket() {
		NBTTagCompound compound = new NBTTagCompound();
		writeToNBT(compound);
		return new Packet132TileEntityData(xCoord, yCoord, zCoord, 1, compound);
	}
	
	@Override
	public void onDataPacket(INetworkManager net, Packet132TileEntityData pckt) {
		readFromNBT(pckt.customParam1);
	}
}
