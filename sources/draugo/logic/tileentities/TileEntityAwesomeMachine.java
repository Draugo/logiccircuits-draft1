package draugo.logic.tileentities;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import draugo.logic.block.BlockAwesomeMachine;

public class TileEntityAwesomeMachine extends ExtendedTileEntity implements IInventory {
	public static final String TE_KEY = "tileentity.awesomemachine";
	
	public static final String TAG_TYPE = "Type";
	public static final String TAG_ENABLED = "Enabled";
	public static final String TAG_ITEMS = "Items";
	public static final String TAG_SLOT = "Slot";
	public static final String TAG_CUSTOM = "Custom";
	public static final String TAG_HEIGHT = "Height";

	// Implement inventory
	private ItemStack[] items;
	private int type;
	private boolean enabled;
	public int heightSetting = 0;

	private byte[] customSetup;
	
	// Cache values
	private int anvils = -1;
	
	public TileEntityAwesomeMachine() {
		items = new ItemStack[3];
		type = 0;
		enabled = false;
		customSetup = new byte[49];
	}
	
	public int getType() {return type;}
	public void setType(int value) {
		if(value >= 0 && value < BlockAwesomeMachine.TEXTURE_SIDES.length) {
			type = value;
			/*if(!worldObj.isRemote) {
				worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
				worldObj.scheduleBlockUpdate(xCoord, yCoord, zCoord, 1, 1);
			}*/
		}
	}
	
	public boolean isEnabled() {return enabled;}
	public void setEnabled(boolean value) {
		enabled = value;
		/*if(!worldObj.isRemote) {
			worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
			worldObj.scheduleBlockUpdate(xCoord, yCoord, zCoord, 1, 1);
		}*/
	}
	
	public byte[] getCustomSetup() {return customSetup;}
	
	@Override
	public int getSizeInventory() {
		return items.length;
	}

	@Override
	public ItemStack getStackInSlot(int i) {
		return items[i];
	}

	@Override
	public ItemStack decrStackSize(int i, int count) {
		ItemStack stack = getStackInSlot(i);
		
		if(stack != null) {
			if(stack.stackSize <= count) {
				setInventorySlotContents(i, null);
			} else {
				stack = stack.splitStack(count);
				onInventoryChanged();
			}
		}
		
		return stack;
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int i) {
		ItemStack stack = getStackInSlot(i);
		setInventorySlotContents(i, null);
		return stack;
	}

	@Override
	public void setInventorySlotContents(int i, ItemStack stack) {
		items[i] = stack;
		
		if(stack != null && stack.stackSize > getInventoryStackLimit()) {
			stack.stackSize = getInventoryStackLimit();
		}
		
		onInventoryChanged();
	}

	@Override
	public String getInvName() {
		return "InventoryAwesomeMachine";
	}

	@Override
	public boolean isInvNameLocalized() {
		return false;
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player) {
		return player.getDistanceSq(xCoord + 0.5D, yCoord + 0.5D, zCoord + 0.5D) <= 64;
	}

	@Override
	public boolean isItemValidForSlot(int i, ItemStack stack) {
		return stack.itemID == Block.anvil.blockID;
	}

	// Chest specific
	@Override
	public void openChest() {}
	
	@Override
	public void closeChest() {}
	
	// Implement NBT writing
	@Override
	public void writeToNBT(NBTTagCompound compound) {
		super.writeToNBT(compound);
		
		compound.setByte(TAG_TYPE, (byte)type);
		compound.setBoolean(TAG_ENABLED, enabled);
		compound.setByteArray(TAG_CUSTOM, customSetup);
		
		NBTTagList items = new NBTTagList();
		for(int i = 0; i < getSizeInventory(); i++) {
			ItemStack stack = getStackInSlot(i);
			
			if(stack != null) {
				NBTTagCompound item = new NBTTagCompound();
				item.setByte(TAG_SLOT, (byte)i);
				stack.writeToNBT(item);
				items.appendTag(item);
			}
		}
		
		compound.setTag(TAG_ITEMS, items);
		
		compound.setByte(TAG_HEIGHT, (byte)heightSetting);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound compound) {
		super.readFromNBT(compound);
		
		type = compound.getByte(TAG_TYPE);
		enabled = compound.getBoolean(TAG_ENABLED);
		byte[] temp = compound.getByteArray(TAG_CUSTOM);
		for(int i = 0; i < temp.length; i++)
			customSetup[i] = temp[i];
		
		NBTTagList items = compound.getTagList(TAG_ITEMS);
		
		for(int i = 0; i < items.tagCount(); i++) {
			NBTTagCompound item = (NBTTagCompound)items.tagAt(i);
			int slot = item.getByte(TAG_SLOT);
			if(slot >= 0 && slot < getSizeInventory()) {
				setInventorySlotContents(slot, ItemStack.loadItemStackFromNBT(item));
			}
		}
		
		heightSetting = compound.getByte(TAG_HEIGHT);
	}
	
	public void receiveInterfaceEvent(byte eventType, byte value) {
		int meta;
		switch(eventType) {
		case 0:
			switch(value) {
			case 0:
				// Old way
				meta = worldObj.getBlockMetadata(xCoord, yCoord, zCoord);
				
				int type = meta / 2;
				int disabled = meta % 2 == 0 ? 1 : 0;
				int newMeta = type * 2 + disabled;
				
				worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, newMeta, 3);
				
				// New way
				//setEnabled(!isEnabled());
				break;
			case 1:
				meta = worldObj.getBlockMetadata(xCoord, yCoord, zCoord);
				
				customSetup = new byte[49];
				
				worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, meta % 2, 3);
				
				break;
			}
			break;
		case 1:
			customSetup[value] = (customSetup[value] == 1) ? (byte)0 : (byte)1;
			break;
		case 2:
			heightSetting = value;
			break;
		}
	}

	private void calculateAnvilCount() {
		anvils = 0;
		for(int i = 0; i < getSizeInventory(); i++) {
			ItemStack stack = getStackInSlot(i);
			if(stack != null && isItemValidForSlot(i, stack)) {
				anvils += stack.stackSize;
			}
		}
	}
	
	public int getAnvils() {
		if(anvils == -1) {
			calculateAnvilCount();
		}
		
		return anvils;
	}
	
	@Override
	public void onInventoryChanged() {
		super.onInventoryChanged();
		
		anvils = -1;
	}
}
