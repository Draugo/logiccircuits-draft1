package draugo.logic.network;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;

import cpw.mods.fml.common.network.IPacketHandler;
import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.common.network.Player;
import draugo.logic.entities.EntitySpaceShip;
import draugo.logic.interfaces.ContainerAwesomeMachine;
import draugo.logic.resources.ModInfo;
import draugo.logic.tileentities.TileEntityAwesomeMachine;

public class PacketHandler implements IPacketHandler {
	
	public static final byte PACKET_SHIP = 0;
	public static final byte PACKET_INTERFACE = 1;
	public static final byte EVENT_TYPE_BUTTON = 0;
	public static final byte EVENT_TYPE_CUSTOM_GRID = 1;
	public static final byte EVENT_TYPE_HEIGHT_SLIDER = 2;
	
	@Override
	public void onPacketData(
			INetworkManager manager,
			Packet250CustomPayload packet,
			Player player) {
		ByteArrayDataInput reader = ByteStreams.newDataInput(packet.data);
		EntityPlayer entityPlayer = (EntityPlayer) player;
		int operation;
		byte packetType = reader.readByte();
		switch(packetType) {
		case 0:
			int shipId = reader.readInt();
			operation = reader.readByte();
			
			Entity entity = entityPlayer.worldObj.getEntityByID(shipId);
			if(entity != null && entity instanceof EntitySpaceShip && entity.riddenByEntity == entityPlayer) {
				((EntitySpaceShip)entity).doDrop(operation);
			}
			break;
		case 1:
			byte type = reader.readByte();
			byte value = reader.readByte();
			
			Container container = entityPlayer.openContainer;
			if(container != null && container instanceof ContainerAwesomeMachine) {
				TileEntityAwesomeMachine machine = ((ContainerAwesomeMachine)container).getMachine();
				machine.receiveInterfaceEvent(type, value);
			}
			break;
		}
	}

	public static void sendShipPacket(EntitySpaceShip ship, byte id) {
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
		DataOutputStream dataStream = new DataOutputStream(byteStream);
		
		try {
			dataStream.writeByte(PACKET_SHIP);
			dataStream.writeInt(ship.entityId);
			dataStream.writeByte(id);
			
			PacketDispatcher.sendPacketToServer(PacketDispatcher.getPacket(ModInfo.MOD_CHANNEL_1, byteStream.toByteArray()));
		} catch(IOException ioe) {
			System.err.append("Failed to send spaceship drop packet!");
		}
	}
	
	public static void sendInterfacePacket(byte type, byte value) {
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
		DataOutputStream dataStream = new DataOutputStream(byteStream);
		
		try {
			dataStream.writeByte(PACKET_INTERFACE);
			dataStream.writeByte(type);
			dataStream.writeByte(value);
			
			PacketDispatcher.sendPacketToServer(PacketDispatcher.getPacket(ModInfo.MOD_CHANNEL_1, byteStream.toByteArray()));
		} catch(IOException ioe) {
			System.err.append("Failed to send awesome machine button click packet!");
		}
	}
	
}
