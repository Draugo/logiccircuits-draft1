package draugo.logic.interfaces;

import java.util.Arrays;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import draugo.logic.block.BlockHandler;
import draugo.logic.network.PacketHandler;
import draugo.logic.resources.ModInfo;
import draugo.logic.tileentities.TileEntityAwesomeMachine;

@SideOnly(Side.CLIENT)
public class GuiAwesomeMachine extends GuiContainer {

	protected TileEntityAwesomeMachine machine;
	private final GuiTab[] tabs;
	private GuiTab activeTab;
	
	public GuiAwesomeMachine(InventoryPlayer invPlayer, TileEntityAwesomeMachine machine) {
		super(new ContainerAwesomeMachine(invPlayer, machine));
		
		this.machine = machine;
		
		xSize = 176;
		ySize = 218;
		
		tabs = new GuiTab[] {
			new GuiTabCustom(8, 80 + 0 * 16, 20, 16),
			new GuiTabHeight(8, 80 + 1 * 16, 20, 16),
			new GuiTabPreview(8, 80 + 2 * 16, 20, 16)
		};
		activeTab = tabs[0];
	}
	
	private static final ResourceLocation texture = new ResourceLocation(ModInfo.ASSET_LOCATION, "textures/gui/machine2.png");
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int x, int y) {
		GL11.glColor4f(1, 1, 1, 1);
		
		// Bind Gui base texture
		Minecraft.getMinecraft().func_110434_K().func_110577_a(texture);
		drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
		
		int meta = machine.worldObj.getBlockMetadata(machine.xCoord, machine.yCoord, machine.zCoord);
		int type = meta / 2;
		if(type != 0) {
			int srcX = 20 * (type - 1);
			int srcY = ySize;
			
			drawTexturedModalRect(guiLeft + 16, guiTop + 42, srcX, srcY, 20, 20);
		}
		
		float filled = machine.getAnvils() / 192F;
		int barHeight = (int)(filled * 27);
		if(barHeight > 0) {
			int srcX = xSize;
			int srcY = 27 - barHeight;
			
			drawTexturedModalRect(guiLeft + 157, guiTop + 67 - barHeight, srcX, srcY, 7, barHeight);
		}
		
		for(GuiRectangle tab : tabs) {
			int srcY = 43;
			
			if(tab == activeTab) {
				srcY += 32;
			} else if(tab.inRect(this, x, y)) {
				srcY += 16;
			}
			
			tab.draw(this, xSize, srcY);
		}
		
		activeTab.drawBackground(this, x, y);
		
		// Bind next texture
		Minecraft.getMinecraft().func_110434_K().func_110577_a(TextureMap.field_110575_b);
		drawTexturedModelRectFromIcon(guiLeft + 63, guiTop + 17, BlockHandler.awesomemachine.getIcon(1, meta), 16, 16);
	}
	
	@Override
	protected void drawGuiContainerForegroundLayer(int x, int y) {
		fontRenderer.drawString("Awesome machine!", 8, 6, 0x404040);
		
		int type = machine.worldObj.getBlockMetadata(machine.xCoord, machine.yCoord, machine.zCoord) / 2;
		
		String str;
		boolean invalid = false;
		
		if(type == 0) {
			str = "No type selected";
		} else {
			int count = 0;
			if(type == 2) {
				count = 8;
			} else if(type == 4) {
				for(int i = 0; i < machine.getCustomSetup().length; i++) {
					if(machine.getCustomSetup()[i] == (byte)1) {
						count++;
					}
				}
			} else {
				count = 20;
			}
			
			if(machine.getAnvils() < count)
				invalid = true;
			
			str = "Requires " + count + " anvils per drop";
		}
		
		int color = invalid ? 0xD30000 : 0x404040;
		fontRenderer.drawSplitString(str, 45, 44, 100, color);
		
		for(GuiTab tab : tabs) {
			if(tab.inRect(this, x, y)) {
				drawHoveringText(tab.getName(), x - guiLeft, y - guiTop);
			}
		}
		
		activeTab.drawForeground(this, x, y);
	}
	
	protected void drawHoveringText(List<String> list, int x, int y) {
		super.drawHoveringText(list, x, y, fontRenderer);
	}
	protected void drawHoveringText(String string, int x, int y) {
		super.drawHoveringText(Arrays.asList(string.split("\n")), x, y, fontRenderer);
	}
	
	public static final String BUTTON_TEXT_ENABLED = "Enabled";
	public static final String BUTTON_TEXT_DISABLED = "Disabled";
	public static final String BUTTON_TEXT_CLEAR = "Clear";
	
	@Override
	public void initGui() {
		super.initGui();
		
		buttonList.clear();
		
		buttonList.add(new GuiButton(0, guiLeft + 80, guiTop + 14, 48, 20,
				machine.getBlockMetadata() % 2 == 1 ? BUTTON_TEXT_ENABLED : BUTTON_TEXT_DISABLED));
		
		GuiButton clearButton = new GuiButton(1, guiLeft + 130, guiTop + 14, 40, 20, BUTTON_TEXT_CLEAR);
		clearButton.enabled = machine.getBlockMetadata() / 2 != 0;
		buttonList.add(clearButton);
	}
	
	@Override
	protected void actionPerformed(GuiButton button) {
		switch(button.id) {
		case 0:
			PacketHandler.sendInterfacePacket(PacketHandler.EVENT_TYPE_BUTTON, (byte)button.id);
			button.displayString = button.displayString.equals(BUTTON_TEXT_ENABLED) ? BUTTON_TEXT_DISABLED : BUTTON_TEXT_ENABLED;
			break;
		case 1:
			PacketHandler.sendInterfacePacket(PacketHandler.EVENT_TYPE_BUTTON, (byte)button.id);
			button.enabled = false;
		}
	}
	
	protected int getLeft() {
		return guiLeft;
	}
	
	protected int getTop() {
		return guiTop;
	}
	
	protected FontRenderer getFontRenderer() {return fontRenderer;}
	
	@Override
	protected void mouseClicked(int x, int y, int button) {
		super.mouseClicked(x, y, button);
		
		activeTab.mouseClick(this, x, y, button);
		
		for(GuiTab tab : tabs) {
			if(activeTab != tab) {
				if(tab.inRect(this, x, y)) {
					activeTab = tab;
					return;
				}
			}
		}
	}
	
	@Override
	protected void mouseClickMove(int x, int y, int button, long timeSinceClicked) {
		super.mouseClickMove(x, y, button, timeSinceClicked);
		
		activeTab.mouseMoveClick(this, x, y, button, timeSinceClicked);
	}
	
	@Override
	protected void mouseMovedOrUp(int x, int y, int button) {
		super.mouseMovedOrUp(x, y, button);
		
		activeTab.mouseReleased(this, x, y, button);
	}
}
