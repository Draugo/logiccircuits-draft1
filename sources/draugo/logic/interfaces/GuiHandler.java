package draugo.logic.interfaces;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import cpw.mods.fml.common.network.IGuiHandler;
import cpw.mods.fml.common.network.NetworkRegistry;
import draugo.logic.LogicCircuitsFirstDraft;
import draugo.logic.tileentities.TileEntityAwesomeMachine;

public class GuiHandler implements IGuiHandler {

	public GuiHandler() {
		NetworkRegistry.instance().registerGuiHandler(LogicCircuitsFirstDraft.instance, this);
	}
	
	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		switch(ID) {
		case 0:
			TileEntity te = world.getBlockTileEntity(x, y, z);
			if(te != null && te instanceof TileEntityAwesomeMachine) {
				return new ContainerAwesomeMachine(player.inventory, (TileEntityAwesomeMachine)te);
			}
			break;
		}
		
		return null;
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		switch(ID) {
		case 0:
			TileEntity te = world.getBlockTileEntity(x, y, z);
			if(te != null && te instanceof TileEntityAwesomeMachine) {
				return new GuiAwesomeMachine(player.inventory, (TileEntityAwesomeMachine)te);
			}
		}
		
		return null;
	}

}
