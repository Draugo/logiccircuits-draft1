package draugo.logic.interfaces;

import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import draugo.logic.network.PacketHandler;

@SideOnly(Side.CLIENT)
public class GuiTabCustom extends GuiTab {
	protected static final GuiRectangle[] rectangles;
	static {
		rectangles = new GuiRectangle[49];
		for(int i = 0; i < 7; i++) {
			for(int j = 0; j < 7; j++) {
				rectangles[i + j*7] = new GuiRectangle(57+i*9, 70 + j*9, 8, 8);
						
			}
		}
	}
	
	
	public GuiTabCustom(int x, int y, int w, int h) {
		super("Custom", x, y, w, h);
	}

	@Override
	public void drawBackground(GuiAwesomeMachine gui, int x, int y) {
		int meta = gui.machine.worldObj.getBlockMetadata(gui.machine.xCoord, gui.machine.yCoord, gui.machine.zCoord);
		int type = meta / 2;
		
		if(type == 4) {
			for(int i = 0; i < rectangles.length; i++) {
				GuiRectangle rect = rectangles[i];
				int srcX = 176;
				int srcY = 27;
				
				if(rect.inRect(gui, x, y))
					srcX += 8;
				
				rect.draw(gui, srcX, srcY);
				
				if(gui.machine.getCustomSetup()[i] == (byte)1)
					rect.draw(gui, 176, 35);
			}
		}
	}

	@Override
	public void drawForeground(GuiAwesomeMachine gui, int x, int y) {
		int meta = gui.machine.worldObj.getBlockMetadata(gui.machine.xCoord, gui.machine.yCoord, gui.machine.zCoord);
		int type = meta / 2;
		
		if(type == 4) {
			for(int i = 0; i < rectangles.length; i++) {
				GuiRectangle rect = rectangles[i];
				if(rect.inRect(gui, x, y)) {
					List<String> lines = new ArrayList<String>();
					if(gui.machine.getCustomSetup()[i] == 1) {
						lines.add(GuiColor.GREEN+"Active");
					} else {
						lines.add(GuiColor.RED + "Inactive");
					}
					lines.add(GuiColor.YELLOW + "Click to change");
					gui.drawHoveringText(lines, x - gui.getLeft(), y - gui.getTop());
				}
			}
		}
	}

	private byte currentMode;
	@Override
	public void mouseClick(GuiAwesomeMachine gui, int x, int y, int button) {
		super.mouseClick(gui, x, y, button);
		for(int i = 0; i < rectangles.length; i++) {
			GuiRectangle rect = rectangles[i];
			if(rect.inRect(gui, x, y)) {
				currentMode = gui.machine.getCustomSetup()[i];
				PacketHandler.sendInterfacePacket(PacketHandler.EVENT_TYPE_CUSTOM_GRID, (byte)(i));
				return;
			}
		}
	}
	
	@Override
	public void mouseMoveClick(GuiAwesomeMachine gui, int x, int y, int button, long timeSinceClicked) {
		super.mouseMoveClick(gui, x, y, button, timeSinceClicked);
		for(int i = 0; i < rectangles.length; i++) {
			GuiRectangle rect = rectangles[i];
			
			if(gui.machine.getCustomSetup()[i] == currentMode && rect.inRect(gui, x, y)) {
				PacketHandler.sendInterfacePacket(PacketHandler.EVENT_TYPE_CUSTOM_GRID, (byte)(i));
				gui.machine.getCustomSetup()[i] = (currentMode == 1) ? (byte)0 : (byte)1;
				break;
			}
		}
	}

}
