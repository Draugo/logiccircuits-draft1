package draugo.logic.interfaces;

import org.bouncycastle.util.Arrays;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import draugo.logic.tileentities.TileEntityAwesomeMachine;

public class ContainerAwesomeMachine extends Container {

	private TileEntityAwesomeMachine machine;

	public ContainerAwesomeMachine(InventoryPlayer invPlayer, TileEntityAwesomeMachine machine) {
		this.machine = machine;
		
		for(int x = 0; x < 9; x++) {
			addSlotToContainer(new Slot(invPlayer, x, 8 + 18*x, 194));
		}
		
		for(int y = 0; y < 3; y++) {
			for(int x = 0; x < 9; x++) {
				addSlotToContainer(new Slot(invPlayer, x + y*9 + 9, 8 + 18*x, 136 + y*18));
			}
		}
		
		for(int x = 0; x < 3; x++) {
			addSlotToContainer(new SlotAnvil(machine, x, 8 + 18*x, 17));
		}
	}
	
	public TileEntityAwesomeMachine getMachine(){return machine;}
	
	@Override
	public boolean canInteractWith(EntityPlayer player) {
		return machine.isUseableByPlayer(player);
	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int slotId) {
		Slot slot = getSlot(slotId);
		if(slot != null && slot.getHasStack()) {
			ItemStack stack = slot.getStack();
			ItemStack result = stack.copy();
			
			if(slotId >= 36) {
				if(!mergeItemStack(stack, 0, 36, false)) {
					return null;
				}
			} else if(stack.itemID != Block.anvil.blockID || !mergeItemStack(stack, 36, 36 + machine.getSizeInventory(), false)) {
				return null;
			}
			
			if(stack.stackSize == 0) {
				slot.putStack(null);
			} else {
				slot.onSlotChanged();
			}
			
			slot.onPickupFromSlot(player, stack);
			
			return result;
		}
		
		return null;
	}
	
	@Override
	public void addCraftingToCrafters(ICrafting player) {
		super.addCraftingToCrafters(player);
		
		for(int i = 0; i < machine.getCustomSetup().length; i++) {
			player.sendProgressBarUpdate(this, i, machine.getCustomSetup()[i]);
		}
		
		player.sendProgressBarUpdate(this, 49, machine.heightSetting);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void updateProgressBar(int id, int data) {
		if(id < machine.getCustomSetup().length) {
			machine.getCustomSetup()[id] = (byte)data;
		} else {
			machine.heightSetting = data;
		}
	}
	
	private byte[] oldData = new byte[49];
	private int oldHeight;
	
	@Override
	public void detectAndSendChanges() {
		super.detectAndSendChanges();
		
		for(Object player : crafters) {
			for(int i = 0; i < oldData.length; i++) {
				if(machine.getCustomSetup()[i] != oldData[i]) {
					((ICrafting)player).sendProgressBarUpdate(this, i, machine.getCustomSetup()[i]);
				}
			}
			if(machine.heightSetting != oldHeight) {
				((ICrafting)player).sendProgressBarUpdate(this, 49, machine.heightSetting);
			}
		}
		
		
		oldData = Arrays.copyOf(machine.getCustomSetup(), machine.getCustomSetup().length);
		oldHeight = machine.heightSetting;
	}

}
