package draugo.logic.interfaces;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityFallingSand;

@SideOnly(Side.CLIENT)
public class GuiTabPreview extends GuiTab {

	private final Entity anvil;
	
	private float yaw;
	private float roll;
	private boolean rollDown;
	
	public GuiTabPreview(int x, int y, int w, int h) {
		super("Anvil preview", x, y, w, h);
		
		anvil = new EntityFallingSand(Minecraft.getMinecraft().theWorld, 0, 0, 0, Block.anvil.blockID);
	}

	@Override
	public void drawBackground(GuiAwesomeMachine gui, int x, int y) {
		GL11.glPushMatrix(); // Add rendering layer
		
		GL11.glTranslatef(gui.getLeft() + 90, gui.getTop() + 100, 100);
		
		float scale = 30F;
		GL11.glScalef(-scale, scale, scale); // you could just scale y*-1 BUT NOOOO
		
		RenderHelper.enableStandardItemLighting();
		
		GL11.glRotatef(180, 0, 0, 1);
		GL11.glRotatef(roll, 1, 0, 0);
		GL11.glRotatef(yaw, 0, 1, 0);
		
		RenderManager.instance.renderEntityWithPosYaw(anvil, 0, 0, 0, 0, 0);
		
		RenderHelper.disableStandardItemLighting();
		GL11.glPopMatrix(); // Remove rendering layer
		
		yaw += 0.5F;
		if(rollDown) {
			roll -= 0.05F;
			if(roll < -5) {
				rollDown = false;
				roll = -5;
			}
		} else {
			roll += 0.05F;
			if(roll > 25) {
				rollDown = true;
				roll = 25;
			}
		}
	}

	@Override
	public void drawForeground(GuiAwesomeMachine gui, int x, int y) {

	}

}
