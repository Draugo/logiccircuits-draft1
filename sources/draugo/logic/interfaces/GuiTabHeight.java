package draugo.logic.interfaces;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import draugo.logic.network.PacketHandler;

@SideOnly(Side.CLIENT)
public class GuiTabHeight extends GuiTab {

	public GuiTabHeight(int x, int y, int w, int h) {
		super("Height", x, y, w, h);
		// TODO Auto-generated constructor stub
	}

	public static final GuiRectangle bar = new GuiRectangle(50, 100, 91, 6);
	public static final GuiRectangle slider = new GuiRectangle(75, 97, 6, 11);
	
	private int tempHeightSetting;
	private boolean isDragging;
	
	@Override
	public void drawBackground(GuiAwesomeMachine gui, int x, int y) {
		bar.draw(gui, 0, 250);
		updateSliderPosition(gui);
		slider.draw(gui, 0, 239);
	}

	@Override
	public void drawForeground(GuiAwesomeMachine gui, int x, int y) {
		gui.getFontRenderer().drawString("Height: " + getCurrentHeight(gui), 60, 88, 0x404040);
	}

	@Override
	public void mouseClick(GuiAwesomeMachine gui, int x, int y, int button) {
		if(button == 0 && slider.inRect(gui, x, y)) {
			isDragging = true;
			tempHeightSetting = gui.machine.heightSetting;
		}
	}

	@Override
	public void mouseMoveClick(GuiAwesomeMachine gui, int x, int y, int button, long timeSinceClicked) {
		if(isDragging) {
			tempHeightSetting = x - gui.getLeft() - 50;
			if(tempHeightSetting < 0) {
				tempHeightSetting = 0;
			} else if(tempHeightSetting > 85) {
				tempHeightSetting = 85;
			}
		}
	}
	
	@Override
	public void mouseReleased(GuiAwesomeMachine gui, int x, int y, int button) {
		if(button == 0 && isDragging) {
			PacketHandler.sendInterfacePacket(PacketHandler.EVENT_TYPE_HEIGHT_SLIDER, (byte)tempHeightSetting);
			gui.machine.heightSetting = tempHeightSetting;
			isDragging = false;
		}
	}
	
	private void updateSliderPosition(GuiAwesomeMachine gui) {
		slider.setX(50 + getCurrentHeight(gui));
	}
	
	private int getCurrentHeight(GuiAwesomeMachine gui) {
		return (isDragging ? tempHeightSetting : gui.machine.heightSetting);
	}
}
