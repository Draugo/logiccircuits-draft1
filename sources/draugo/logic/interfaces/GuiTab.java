package draugo.logic.interfaces;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public abstract class GuiTab extends GuiRectangle {

	private String name;
	
	public GuiTab(String name, int x, int y, int w, int h) {
		super(x, y, w, h);
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public abstract void drawBackground(GuiAwesomeMachine gui, int x, int y);
	public abstract void drawForeground(GuiAwesomeMachine gui, int x, int y);
	public void mouseClick(GuiAwesomeMachine gui, int x, int y, int button) {};
	public void mouseMoveClick(GuiAwesomeMachine gui, int x, int y, int button, long timeSinceClicked) {}
	public void mouseReleased(GuiAwesomeMachine gui, int x, int y, int button) {}
}
