package draugo.logic.interfaces;


public class GuiRectangle {
	private int x;
	private int y;
	private int w;
	private int h;
	
	public GuiRectangle(int x, int y, int w, int h) {
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
	}
	
	public boolean inRect(GuiAwesomeMachine gui, int x, int y) {
		x -= gui.getLeft();
		y -= gui.getTop();
		return (this.x <= x && x <= this.x + this.w && this.y <= y && y <= this.y + this.h);
	}
	
	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void draw(GuiAwesomeMachine gui, int srcX, int srcY) {
		gui.drawTexturedModalRect(x + gui.getLeft(), y + gui.getTop(), srcX, srcY, w, h);
	}
}
