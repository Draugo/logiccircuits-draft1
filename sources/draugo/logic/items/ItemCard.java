package draugo.logic.items;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import draugo.logic.block.BlockAwesomeMachine;
import draugo.logic.resources.ModInfo;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.world.World;

public class ItemCard extends Item {
	// Configuration
	public static int ITEM_ID;
	public static final String CONFIG_KEY = "item.card";
	public static final int DEFAULT_ID = 24202;
	
	public static final String UNLOCALIZED_NAME = "draugo.logic.item.symbolCard";
	public static final String[] NAMES = {"Card: Left", "Card: Equal", "Card: Right", "Card: Custom"};
	
	public static final String[] ICONS = {"card_left", "card_equal", "card_right", "card_custom"};
	
	// Item implementation
	@SideOnly(Side.CLIENT)
	private Icon[] icons;
	
	public ItemCard(int id) {
		super(id);
		setCreativeTab(CreativeTabs.tabMisc);
		setHasSubtypes(true);
	}
	
	@Override
	public String getUnlocalizedName(ItemStack is) {
		return UNLOCALIZED_NAME + is.getItemDamage();
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register) {
		icons = new Icon[ICONS.length];
		for(int i = 0; i<ICONS.length; i++) {
			icons[i] = register.registerIcon(ModInfo.ASSET_LOCATION + ":" + ICONS[i]);
		}
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public Icon getIconFromDamage(int dmg) {
		return icons[dmg];
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void getSubItems(int id, CreativeTabs tab, List list) {
		for(int i = 0; i < NAMES.length; i++) {
			ItemStack stack = new ItemStack(id, 1, i);
			list.add(stack);
		}
	}
	
	@Override
	public boolean onItemUseFirst(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ) {
		if(!world.isRemote && world.getBlockId(x, y, z) == BlockAwesomeMachine.BLOCK_ID) {
			int meta = world.getBlockMetadata(x, y, z);
			int blockType = meta / 2;
			int type = stack.getItemDamage() + 1;
			if(blockType != type) {
				int disabled = meta % 2;
				
				int newMeta = type * 2 + disabled;
				
				world.setBlockMetadataWithNotify(x, y, z, newMeta, 3);
				stack.stackSize--;
			}
			
			return true;
		}
		
		return false;
	}
}
