package draugo.logic.items;

import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import draugo.logic.client.sounds.Sounds;
import draugo.logic.entities.EntitySpaceShip;
import draugo.logic.resources.ModInfo;

public class ItemAwesomeWand extends Item {
	// Configuration
	public static int ITEM_ID;
	public static final String CONFIG_KEY = "item.awesomewand";
	public static final int DEFAULT_ID = 24201;
	
	public static final String UNLOCALIZED_NAME = "draugo.logic.item.awesomeWand";
	public static final String NAME = "Awesome wand";
	
	public static final String ICON = "awesome_wand";
	public static final String ICON_DOWNVOTE = "awesome_wand_downvote";
	
	// Item implementation
	@SideOnly(Side.CLIENT)
	private Icon downvoteIcon;
	@SideOnly(Side.CLIENT)
	private Icon normalIcon;

	public ItemAwesomeWand(int id) {
		super(id);
		setCreativeTab(CreativeTabs.tabCombat);
		setMaxStackSize(1);
		setUnlocalizedName(UNLOCALIZED_NAME);
	}
	
	@Override
	public boolean func_111207_a(ItemStack is, EntityPlayer player, EntityLivingBase target) {
		if(!target.worldObj.isRemote) {
			if(isOverused(is.getItemDamage())) {
				is.setItemDamage(is.getItemDamage() - 1);
				Sounds.WAND_USE.play(target.posX, target.posY, target.posZ, 1, 0);
			} else {
				target.motionY = 2;
				Sounds.WAND_USE.play(target.posX, target.posY, target.posZ, 1, 3);
				is.setItemDamage(is.getItemDamage() + 1);
			}
			
		}
		
		return false;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register) {
		normalIcon = register.registerIcon(ModInfo.ASSET_LOCATION + ":" + ICON);
		downvoteIcon = register.registerIcon(ModInfo.ASSET_LOCATION + ":" + ICON_DOWNVOTE);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack is, EntityPlayer player, List info, boolean useExtraInformation) {
		info.add("You have launched "+is.getItemDamage()+" poor souls to their doom.");
		
		if(isOverused(is.getItemDamage())) {
			info.add("Stop your maddness!");
		}
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public Icon getIconFromDamage(int dmg) {
		return (isOverused(dmg))?downvoteIcon:normalIcon;
	}
	
	private boolean isOverused(int dmg) {
		return dmg >= 10;
	}
	
	@Override
	public boolean onItemUseFirst(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ) {
		if(!world.isRemote && player.isSneaking() && side == 1) {
			EntitySpaceShip ship = new EntitySpaceShip(world);
			
			ship.posX = x + 0.5;
			ship.posY = y + 1.5;
			ship.posZ = z + 0.5;
			
			if(isOverused(stack.getItemDamage())) {
				stack.setItemDamage(0);
				ship.setCharged();
			} else {
				stack.setItemDamage(stack.getItemDamage()+1);
			}
			
			world.spawnEntityInWorld(ship);
			
			return true;
		} else {
			return false;
		}
	}
}
