package draugo.logic.items;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import draugo.logic.entities.EntityDroid;
import draugo.logic.resources.ModInfo;

public class ItemDroid extends Item {
	public static int ITEM_ID;
	public static final String CONFIG_KEY = "item.droid";
	public static final int DEFAULT_ID = 24203;
	
	public static final String UNLOCALIZED_NAME = "draugo.logic.item.droid";
	public static final String NAME = "Droid";
	
	public static final String ICON = "droidItem";
	
	public ItemDroid(int id) {
		super(id);
		
		setCreativeTab(CreativeTabs.tabMisc);
		setUnlocalizedName(UNLOCALIZED_NAME);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register) {
		itemIcon = register.registerIcon(ModInfo.ASSET_LOCATION + ":" + ICON);
	}
	
	@Override
	public boolean onItemUseFirst(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ) {
		if(!world.isRemote) {
			world.spawnEntityInWorld(new EntityDroid(world, x + 0.5D, y + 1.5D, z + 0.5D));
			stack.stackSize--;
			
			return true;
		} else {
			return false;
		}
	}
}
