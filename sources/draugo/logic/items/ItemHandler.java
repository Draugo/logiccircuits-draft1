package draugo.logic.items;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemHandler {
	public static Item awesomewand;
	public static Item card;
	public static Item droid;
	public static Item charger;
	
	public static void init() {
		awesomewand	= new ItemAwesomeWand(ItemAwesomeWand.ITEM_ID);
		card		= new ItemCard(ItemCard.ITEM_ID);
		droid		= new ItemDroid(ItemDroid.ITEM_ID);
		charger		= new ItemColorCharger(ItemColorCharger.ITEM_ID);
	}
	
	public static void addNames() {
		LanguageRegistry.addName(awesomewand, ItemAwesomeWand.NAME);
		for(int i = 0; i < ItemCard.NAMES.length; i++) {
			LanguageRegistry.addName(new ItemStack(card, 1, i), ItemCard.NAMES[i]);
		}
		LanguageRegistry.addName(droid, ItemDroid.NAME);
		LanguageRegistry.addName(charger, ItemColorCharger.NAME);
	}
	
	public static void registerRecipes() {
		GameRegistry.addRecipe(
				new ItemStack(awesomewand),
				new Object[] {	" XX",
								" /X",
								"/  ",
								
								'X', Item.gunpowder,
								'/', Item.stick});
	}
}
