package draugo.logic.items;

import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import draugo.logic.block.BlockColor;
import draugo.logic.interfaces.GuiColor;
import draugo.logic.resources.ModInfo;

public class ItemColorCharger extends Item {

	private static final byte MASK_ACTIVE = 0b11;
	private static final short MASK_COLORS = 0b11111111111100;
	private static final byte MASK_COLOR = 0b111;
	
	
	public static int ITEM_ID;
	public static final String CONFIG_KEY = "item.colorCharger";
	public static final int DEFAULT_ID = 24204;
	
	public static final String UNLOCALIZED_NAME = "draugo.logic.item.colorCharger";
	public static final String NAME = "Color Charger";
	
	public static final String TEXTURE = "charger_";
	public static final String[] TEXTURE_COLORS = {"White", "Red", "Green", "Blue"};
	public static final GuiColor[] COLOR_TAGS = {GuiColor.WHITE, GuiColor.RED, GuiColor.GREEN, GuiColor.BLUE};
	
	public ItemColorCharger(int id) {
		super(id);
		setCreativeTab(CreativeTabs.tabMisc);
		setHasSubtypes(true);
		setMaxStackSize(1);
		setUnlocalizedName(UNLOCALIZED_NAME);
	}

	public int getUsesForColor(int dmg, int color) {
		int colors = (dmg & MASK_COLORS) >> 2;
		return (colors >> (color * 3)) & MASK_COLOR;
	}
	
	public int getActiveColor(int dmg) {
		return dmg & MASK_ACTIVE;
	}
	
	public int reduceActiveUses(int dmg) {
		int activeColor = getActiveColor(dmg);
		int uses = getUsesForColor(dmg, activeColor) - 1;
		
		dmg &= ~((MASK_COLOR << 2) << (activeColor * 3));
		dmg |= uses << (activeColor * 3 + 2);
		
		return dmg;
	}
	
	@SideOnly(Side.CLIENT)
	private Icon[] icons;
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister register) {
		icons = new Icon[TEXTURE_COLORS.length];
		for(int i = 0; i<icons.length; i++) {
			icons[i] = register.registerIcon(ModInfo.ASSET_LOCATION + ":" + TEXTURE+TEXTURE_COLORS[i]);
		}
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public Icon getIconFromDamage(int dmg) {
		return icons[getActiveColor(dmg)];
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean useExtraInformation) {
		list.add("Active color: " + COLOR_TAGS[getActiveColor(stack.getItemDamage())] + TEXTURE_COLORS[getActiveColor(stack.getItemDamage())]);
		for(int i = 0; i < TEXTURE_COLORS.length; i++) {
			int uses = getUsesForColor(stack.getItemDamage(), i);
			if(uses > 0)
				list.add(COLOR_TAGS[getActiveColor(i)] + TEXTURE_COLORS[i] + ": " + uses);
		}
	}
	
	public int switchToNextColor(int dmg) {
		int nextColor = (dmg + 1) & MASK_ACTIVE;
		dmg &= ~MASK_ACTIVE;
		dmg |= nextColor;
		return dmg;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void getSubItems(int id, CreativeTabs tab, List list) {
		list.add(new ItemStack(id, 1, 0b11111111111100));
	}
	
	@Override
	public boolean onItemUseFirst(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ) {
		if(!world.isRemote) {
			if(player.isSneaking()) {
				int dmg = stack.getItemDamage();
				int startColor = getActiveColor(dmg);
				int curColor = startColor;
				do {
					dmg = switchToNextColor(dmg);
					curColor = getActiveColor(dmg);
				} while(getUsesForColor(dmg, curColor) == 0 && curColor != startColor);
				
				stack.setItemDamage(dmg);
				
				return true;
			} else if(world.getBlockId(x, y, z) == BlockColor.BLOCK_ID) {
				int meta = world.getBlockMetadata(x, y, z);
				
				int itemColor = getActiveColor(stack.getItemDamage());
				if(getUsesForColor(stack.getItemDamage(), itemColor) > 0 && (meta & 0b0011) != itemColor) {
					meta &= ~0b0011;
					meta |= itemColor;
					
					world.setBlockMetadataWithNotify(x, y, z, meta, 3);
					
					stack.setItemDamage(reduceActiveUses(stack.getItemDamage()));
					if((stack.getItemDamage() & 0b11111111111100) == 0) {
						stack.stackSize--;
					}
					
					return true;
				}
			}
		}
		
		return false;
	}
}
