package draugo.logic.items;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import draugo.logic.tileentities.TileEntityAwesomeMachine;

public class ItemAwesomeMachine extends ItemBlock {
	public ItemAwesomeMachine(int id) {
		super(id);
		setFull3D();
		setHasSubtypes(true);
	}
	
	@Override
	public boolean placeBlockAt(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ, int metadata) {
		boolean isSuccess = super.placeBlockAt(stack, player, world, x, y, z, side, hitX, hitY, hitZ, metadata);
		if(!world.isRemote && isSuccess && stack.hasTagCompound()) {
			TileEntity te = world.getBlockTileEntity(x, y, z);
			if(te != null && te instanceof TileEntityAwesomeMachine) {
				TileEntityAwesomeMachine machine = (TileEntityAwesomeMachine) te;
				machine.setType(stack.getTagCompound().getByte(TileEntityAwesomeMachine.TAG_TYPE));
				machine.setEnabled(stack.getTagCompound().getBoolean(TileEntityAwesomeMachine.TAG_ENABLED));
			}
		}
		
		return isSuccess;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public boolean requiresMultipleRenderPasses() {
		return true;
	}
	
	@Override
	public Icon getIcon(ItemStack stack, int pass) {
		System.out.println("Rendering item pass: " + pass);
		return super.getIcon(stack, pass);
	}
	
	@Override
	public int getMetadata(int dmg) {
		return dmg;
	}
}
