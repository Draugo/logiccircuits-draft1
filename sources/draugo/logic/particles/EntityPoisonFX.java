package draugo.logic.particles;

import draugo.logic.block.BlockHandler;
import net.minecraft.client.particle.EntityFX;
import net.minecraft.world.World;

public class EntityPoisonFX extends EntityFX {

	public EntityPoisonFX(World world, double x, double y, double z, double mX, double mY, double mZ) {
		super(world, x, y, z, mX, mY, mZ);
		
		func_110125_a(BlockHandler.poison.poisonParticle);
		
		particleScale = 3;
		particleAlpha = rand.nextFloat();
		particleBlue = 0;
		particleGreen = 0.7f;
	}
	
	@Override
	public int getFXLayer() {
		return 1;
	}
	
	@Override
	public void onUpdate() {
		super.onUpdate();
		
		particleScale = (1 - (float)particleAge / particleMaxAge) * 3;
	}

}
