package draugo.logic.world;

import java.util.Random;

import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraft.world.gen.feature.WorldGenerator;
import cpw.mods.fml.common.IWorldGenerator;
import cpw.mods.fml.common.registry.GameRegistry;
import draugo.logic.block.BlockPoison;

public class GenerationHandler implements IWorldGenerator {

	private WorldGenerator poisonGen;
	
	public GenerationHandler() {
		GameRegistry.registerWorldGenerator(this);
		poisonGen = new WorldGenMinable(BlockPoison.BLOCK_ID, 16);
	}
	
	private void generateStandardOre(Random rand, int chunkX, int chunkZ, World world, int iterations, WorldGenerator gen, int lowY, int highY) {
		for(int i = 0; i < iterations; i++) {
			int x = chunkX*16 + rand.nextInt(16);
			int y = rand.nextInt(highY - lowY) + lowY;
			int z = chunkZ*16 + rand.nextInt(16);
			
			gen.generate(world, rand, x, y, z);
		}
	}

	@Override
	public void generate(Random rand, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
		generateStandardOre(rand, chunkX, chunkZ, world, 20, poisonGen, 0, 128);
	}
}
