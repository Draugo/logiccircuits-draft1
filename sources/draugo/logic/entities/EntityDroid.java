package draugo.logic.entities;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class EntityDroid extends Entity {

	private static final String TAG_START_Y = "StartY";
	private static final String TAG_TARGET_Y = "TargetY";
	
	private double startY;
	private double targetY;
	private float coreRotation;
	private float rotationSpeed;
	private float floatSpeed;
	private float helmetSpeed;
	private float solarPanelRotation;
	private float outerSolarPanelRotation = -(float)Math.PI / 2;
	private float helmetPositionRotation;
	private float r;
	private float g;
	private float b;
	
	public EntityDroid(World world) {
		super(world);
		
		coreRotation = (float)(world.rand.nextDouble() * (double) (Math.PI) * 2);
		rotationSpeed = ((world.rand.nextFloat() / 2 + 0.5F) * 0.05F) * (world.rand.nextBoolean()?-1:1);
		floatSpeed = ((world.rand.nextFloat() / 2 + 0.5F) * 0.05F);
		helmetSpeed = ((world.rand.nextFloat() / 2 + 0.5F) * 0.04F);
		
		r = world.rand.nextFloat();
		g = world.rand.nextFloat();
		b = world.rand.nextFloat();
	}
	
	public EntityDroid(World world, double x, double y, double z) {
		this(world);
		posX = x;
		startY = posY = y;
		posZ = z;
	}

	@Override
	protected void entityInit() {
		dataWatcher.addObject(20, (byte)0);
	}

	@Override
	public void onUpdate() {
		super.onUpdate();
		
		if(!worldObj.isRemote) {
			if(targetY == 0 || Math.abs(posY - targetY) < 0.25) {
				targetY = startY + worldObj.rand.nextDouble()*5;
			}
			
			if(posY < targetY) {
				motionY = floatSpeed;
			} else {
				motionY = floatSpeed * -1;
			}
			
			boolean light = worldObj.getBlockLightValue((int)posX, (int)posY, (int)posZ) >= 15;
			dataWatcher.updateObject(20, light ? (byte)1 : (byte)0);
			
		} else {
			coreRotation += rotationSpeed;
			helmetPositionRotation += helmetSpeed;
			if(dataWatcher.getWatchableObjectByte(20) != 0) {
				float openInnerRotation = (float)Math.PI / 2;
				if(solarPanelRotation != openInnerRotation) {
					solarPanelRotation = Math.min(openInnerRotation, solarPanelRotation + 0.02F);
				} else {
					outerSolarPanelRotation = Math.min(0, outerSolarPanelRotation + 0.02F);
				}
			} else {
				float closedOuterRotation = -(float)Math.PI / 2;
				if(outerSolarPanelRotation != closedOuterRotation) {
					outerSolarPanelRotation = Math.max(closedOuterRotation, outerSolarPanelRotation - 0.02F);
				} else {
					solarPanelRotation = Math.max(0, solarPanelRotation - 0.02F);
				}
			}
		}
		
		setPosition(posX + motionX, posY + motionY, posZ + motionZ);
	}
	
	@Override
	protected void readEntityFromNBT(NBTTagCompound compound) {
		startY = compound.getDouble(TAG_START_Y);
		targetY = compound.getDouble(TAG_TARGET_Y);
	}

	@Override
	protected void writeEntityToNBT(NBTTagCompound compound) {
		compound.setDouble(TAG_START_Y, startY);
		compound.setDouble(TAG_TARGET_Y, targetY);
	}

	public float getCoreRotation() {
		return coreRotation;
	}

	public float getSolarPanelRotation() {
		return solarPanelRotation;
	}

	public float getOuterSolarPanelRotation() {
		return outerSolarPanelRotation;
	}

	public float getHelmetPosition() {
		return -6 - (Math.abs((float)Math.sin(helmetPositionRotation)) * 5.5F);
	}

	public float getRedColor() {
		return r;
	}
	public float getGreenColor() {
		return g;
	}
	public float getBlueColor() {
		return b;
	}

}
