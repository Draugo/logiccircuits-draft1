package draugo.logic.entities;

import cpw.mods.fml.common.registry.EntityRegistry;
import draugo.logic.LogicCircuitsFirstDraft;

public class EntityHandler {
	public static void init() {
		EntityRegistry.registerModEntity(EntitySpaceShip.class, "EntitySpaceShip", 0, LogicCircuitsFirstDraft.instance, 80, 3, true);
		EntityRegistry.registerModEntity(EntityBomb.class, "EntityBomb", 1, LogicCircuitsFirstDraft.instance, 80, 3, false);
		EntityRegistry.registerModEntity(EntityDroid.class, "EntityDroid", 2, LogicCircuitsFirstDraft.instance, 80, 3, true);
	}
}
