package draugo.logic.config;

import java.io.File;

import net.minecraftforge.common.Configuration;
import draugo.logic.block.BlockAwesomeMachine;
import draugo.logic.block.BlockBombBox;
import draugo.logic.block.BlockColor;
import draugo.logic.block.BlockPoison;
import draugo.logic.items.ItemAwesomeWand;
import draugo.logic.items.ItemCard;
import draugo.logic.items.ItemColorCharger;
import draugo.logic.items.ItemDroid;

public class ConfigHandler {
	public static void init(File file) {
		Configuration config = new Configuration(file);
		
		config.load();
		
		// Blocks
		BlockAwesomeMachine.BLOCK_ID = config.getBlock(BlockAwesomeMachine.CONFIG_KEY, BlockAwesomeMachine.DEFAULT_ID).getInt();
		BlockBombBox.BLOCK_ID = config.getBlock(BlockBombBox.CONFIG_KEY, BlockBombBox.DEFAULT_ID).getInt();
		BlockPoison.BLOCK_ID = config.getBlock(BlockPoison.CONFIG_KEY, BlockPoison.DEFAULT_ID).getInt();
		BlockColor.BLOCK_ID = config.getBlock(BlockColor.CONFIG_KEY, BlockColor.DEFAULT_ID).getInt();
		
		// Items, reduce by 256 because fuck minecraft.
		ItemAwesomeWand.ITEM_ID = config.getItem(ItemAwesomeWand.CONFIG_KEY, ItemAwesomeWand.DEFAULT_ID).getInt() - 256;
		ItemCard.ITEM_ID = config.getItem(ItemCard.CONFIG_KEY, ItemCard.DEFAULT_ID).getInt() - 256;
		ItemDroid.ITEM_ID = config.getItem(ItemDroid.CONFIG_KEY, ItemDroid.DEFAULT_ID).getInt() - 256;
		ItemColorCharger.ITEM_ID = config.getItem(ItemColorCharger.CONFIG_KEY, ItemColorCharger.DEFAULT_ID).getInt() - 256;
		
		config.save();
	}
}
