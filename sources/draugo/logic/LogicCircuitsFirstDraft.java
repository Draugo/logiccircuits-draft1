package draugo.logic;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import draugo.logic.block.BlockHandler;
import draugo.logic.config.ConfigHandler;
import draugo.logic.entities.EntityHandler;
import draugo.logic.interfaces.GuiHandler;
import draugo.logic.items.ItemHandler;
import draugo.logic.network.PacketHandler;
import draugo.logic.proxies.CommonProxy;
import draugo.logic.resources.ModInfo;
import draugo.logic.world.GenerationHandler;

@Mod(
		modid = ModInfo.MOD_ID,
		name = ModInfo.MOD_NAME,
		version = ModInfo.MOD_VERSION)
@NetworkMod(
		channels = {ModInfo.MOD_CHANNEL_1},
		clientSideRequired = true,
		serverSideRequired = false,
		packetHandler = PacketHandler.class)
public class LogicCircuitsFirstDraft {

	@Instance(ModInfo.MOD_ID)
	public static LogicCircuitsFirstDraft instance;
	
	@SidedProxy(
			clientSide = "draugo.logic.proxies.ClientProxy",
			serverSide = "draugo.logic.proxies.CommonProxy")
	public static CommonProxy proxy;
	
	@EventHandler
	public void preInitEventHandler(FMLPreInitializationEvent event) {
		ConfigHandler.init(event.getSuggestedConfigurationFile());
		
		BlockHandler.init();
		
		ItemHandler.init();
		
		proxy.initSounds();
		proxy.initRenderers();
	}
	
	@EventHandler
	public void initEventHandler(FMLInitializationEvent event) {
		BlockHandler.addNames();
		BlockHandler.registerTileEnetities();
		
		ItemHandler.addNames();
		ItemHandler.registerRecipes();
		
		EntityHandler.init();
		
		new GenerationHandler();
		new GuiHandler();
	}
	
	@EventHandler
	public void postInitEventHandler(FMLPostInitializationEvent event) {
		
	}
	
}
