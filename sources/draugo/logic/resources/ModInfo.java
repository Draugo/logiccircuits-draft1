package draugo.logic.resources;

public class ModInfo {
	public static final String MOD_ID = "draugo_LogicCircuits";
	public static final String MOD_NAME = "Logic Circuits - First Draft";
	public static final String MOD_VERSION = "Draft 1";
	public static final String MOD_CHANNEL_1 = "draugo.lc.common";
	
	public static final String ASSET_LOCATION = "logic";
}
