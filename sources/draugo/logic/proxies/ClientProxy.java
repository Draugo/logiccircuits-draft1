package draugo.logic.proxies;

import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;
import draugo.logic.block.BlockBombBox;
import draugo.logic.client.ModelDroid;
import draugo.logic.client.RenderAwesomeMachine;
import draugo.logic.client.RenderBombBlock;
import draugo.logic.client.RenderBombBlockItem;
import draugo.logic.client.RenderDroid;
import draugo.logic.client.RenderDroidItem;
import draugo.logic.client.RenderSpaceship;
import draugo.logic.client.sounds.SoundHandler;
import draugo.logic.entities.EntityDroid;
import draugo.logic.entities.EntitySpaceShip;
import draugo.logic.items.ItemDroid;
import draugo.logic.tileentities.TileEntityBombBox;

public class ClientProxy extends CommonProxy {
	@Override
	public void initSounds() {
		new SoundHandler();
	}

	@Override
	public void initRenderers() {
		RenderingRegistry.registerEntityRenderingHandler(EntitySpaceShip.class, new RenderSpaceship());
		
		// Entity renderers
		ModelDroid model = new ModelDroid();
		RenderingRegistry.registerEntityRenderingHandler(EntityDroid.class, new RenderDroid(model));
		MinecraftForgeClient.registerItemRenderer(ItemDroid.ITEM_ID + 256, new RenderDroidItem(model)); // Fuck item ID:s man.
		
		RenderAwesomeMachine machineRender = new RenderAwesomeMachine();
		RenderAwesomeMachine.RENDER_ID = machineRender.getRenderId();
		RenderingRegistry.registerBlockHandler(machineRender);
		
		IModelCustom modelBomb = AdvancedModelLoader.loadModel("/assets/logic/models/bomb.obj");
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityBombBox.class, new RenderBombBlock(modelBomb));
		MinecraftForgeClient.registerItemRenderer(BlockBombBox.BLOCK_ID, new RenderBombBlockItem(modelBomb)); // Fuck item ID:s man.
	}
}
