package draugo.logic.proxies;

public class CommonProxy {

	public void initSounds() {
		// No sounds on server, override in ClientProxy
	}

	public void initRenderers() {
		// No renderers on server, override in ClientProxy
	}

}
